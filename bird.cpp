#include <stdio.h>
#include <stdlib.h>     
#include <conio.h>    
#include <windows.h>

int bird_x,bird_y;							//小鸟坐标
int fast;									//速度
int width,high;								//边框
int safe_up,safe_down,Up,Down,distance;		//安全区，上墙，下墙,距离原点的长度

void HideCursor()//隐藏光标，避免光标闪烁  
{
	//关闭光标
	CONSOLE_CURSOR_INFO cci;    
	cci.bVisible = FALSE;    
	cci.dwSize = sizeof(cci);    
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);    
	SetConsoleCursorInfo(handle, &cci);
}

void makeNum()//随机制造墙和距离
{
	SYSTEMTIME sys;   
	GetLocalTime(&sys);
	char tmp[64]={0};
	int nUp;
	sprintf(tmp,"%02d",sys.wSecond);
	nUp = atoi(tmp);//上限
	int nJL = nUp;//距离
	
	nUp = nUp%10;
	nJL = nJL%30;
	
	Up = nUp;
	safe_up = Up+1;
	safe_down = safe_up+3;
	Down = safe_down+1;
	distance = nJL;
}    
  

void ready()//初始化
{
	bird_x = 1;
	bird_y = 5;
	width = 20;
	high = 15;
	makeNum();
	fast = 200;
	
}

void birdUpdate()//内部更新
{
	bird_y ++;
	distance --;
	if(distance <= 1 && (bird_y <= Up || bird_y >= Down))//撞到墙
	{
		system("cls");
		printf("Game over!");
		getch();
		exit(0);
	}
	else if(distance == -1)
	{
		makeNum();
	}
	
	Sleep(fast);
	fast -= 10;
	if(fast<=10)
	{
		fast = 200;
	}
}

void print()//打印界面
{
	system("cls");
	int i,j;
	for(i=0;i<=high;i++)//---
	{
		for(j=0;j<=width;j++)//|||
		{
			if(i==bird_y && j==bird_x)
				printf(">");
			else if(j==distance && (i<=Up || i>=Down))
				printf("*");
			else
				printf(" ");
		}
		printf("\n");
	}
}

void userUpdate() //用户输入所致的更新
{
	//printf("123");
	char chInput;
	if(kbhit())
	{
		chInput = getch();
		if(chInput == ' ')
			bird_y = bird_y - 2;
	}
}

int main()
{
	HideCursor();
	ready();
	while(1)
	{
		print();
		//getch();
		birdUpdate();
		userUpdate();
	}
	
	return 0;
}


      





